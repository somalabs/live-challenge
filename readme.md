# Desafio Desenvolvedor Front-end

## Instruções
* Dê um fork neste projeto;
* Desenvolva o fluxo proposto nas telas do WebApp:
    * Layout Desktop: https://xd.adobe.com/view/524b709f-9b11-4ead-af03-364b2512c2cf-0718/
    * Layout Mobile: https://xd.adobe.com/view/3601d22f-d640-4bc3-946e-0a50e6dd7e4b-6352/
* Atualize o readme com as instruções necessárias para rodar o seu código;
* Faça um pull request.

##### Sugestões de implementação
* Interação com JSON para renderizar os produtos (você vai encontrar um mockup em /data/mock.json)
* Layout responsivo
* Renderização do player de Vídeo
* Busca funcional
* Micro animações e micro interações

##### Dicas
* Evite usar linguagens, ferramentas e metodologias que não domine;
* Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;
* Se for implementar algum componente que não está nas telas, tente manter a consistência do layout

###### Dúvidas: nathan.miranda@somagrupo.com.br